<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
* Rest API Request , auth  & Route
*/ 
Route::group(['prefix' => 'api/v1'], function()
{   
    //
});    

/*
* Admin Based Auth
*/  

Route::get('/login','Adminauth\AuthController@showLoginForm'); 
Route::post('/register','UserController@signup'); 
Route::post('password/reset','Adminauth\AuthController@resetPassword'); 


    

    